/*
 * BSD Sockets example - Lab 5 - week 6
 * bsd-client.c
 * Demetrios Christou
 *
 * */


#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>

int main(int argc , char *argv[])
{
    int socket_fd , client_socket, c, read_size;
    struct sockaddr_in server, client;
    char client_message[2000];

    //create socket, TCP
    socket_fd = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_fd == -1)
    {
        perror("create socket");
    }
    puts("socket created");

    // configure struct, any inc address, TCP, IPV4, port
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(8888);

    //Bind
    if (bind(socket_fd,(struct sockaddr *)&server , sizeof(server)) < 0) {
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");

    //Listen
    listen(socket_fd , 3);

    //Accept and incoming connection
    puts("Waiting for incoming connections ");
    c = sizeof(struct sockaddr_in);

    //accept connection from an incoming client
    client_socket = accept(socket_fd, (struct sockaddr *)&client, (socklen_t*)&c);
    if (client_socket < 0)
    {
        perror("accept failed");
        return 1;
    }
    puts("Connection accepted ");

    //Receive a message from client
    do
    {
        memset(client_message, 0, sizeof client_message);                   // clear contents of message for new one
        read_size = (int) recv(client_socket , client_message, 2000, 0);  // receive from client and check return value for error
        if(read_size == 0) {
            puts("Client disconnected, exiting....");
            return 2;
        }

        else if(read_size == -1) {
            perror("receive failed");
            return 3;
        }

        printf("client message: ");
        printf ("%s \n", client_message);

    } while(read_size > 0);

    return 0;
}
