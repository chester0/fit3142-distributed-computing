/*
 * BSD Sockets example - Lab 5 - week 6
 * bsd-client.c
 * Demetrios Christou
 *
 * */

#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include <unistd.h>

int main(int argc , char *argv[]) {
    int my_socket;
    struct sockaddr_in server;
    char message[2000];

    //Create socket - AF_INET = IPV4, SOCK_STREAM = TCP
    my_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (my_socket == -1) {
        printf("Could not create socket");
    }
    puts("Socket created");

    // CONFIGURE SERVER STRUCT, IPV4, lochalhost for ip, and IPV4M, PORT = 8888
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons(8888);

    //Connect to remote server
    if (connect(my_socket, (struct sockaddr *) &server, sizeof(server)) < 0) {
        perror("connect failed. Error");
        return 1;
    }
    puts("Connected\n");

    // MAIN LOOP
    do {
        memset(message, 0, 2000);                       // clear contents of message for new one
        printf("Enter message or CTRL+D to exit: ");

        fgets(message, 2000, stdin);                    // read from input

        //Send some data, check for return value
        if (send(my_socket, message, strlen(message), 0) < 0) {
            puts("Send failed");
            return 1;
        }

    } while (message[0] != '\0');                       // while we don't have end of file as first char

    close(my_socket);                                   // close the socket
    return 0;
}
