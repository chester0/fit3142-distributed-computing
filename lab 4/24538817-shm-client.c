/*
 * Shared Memory Client Process - Lab 4 - week 5
 * 24538817-shm-client.c
 * Demetrios Christou
 * modified template Monash uni FIT3142

 */
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "segment-lock.h"

int main()
{
	int
		up = 0, shmid;
	key_t
		mykey;
	void *
		shmat(int _shmid, const void *addr, int flag);
	int
		shmdt(const void *shmaddr);
	SEG_DATA
		*shm, *mydata;

  // The shared memory segment is identified by SEGMENTID
	mykey = SEGMENTID;

  //create the shared memory segment via `shmget' system call.
  //for debugging remove the IPC_EXCL flag - `IPC_CREAT | IPC_EXCL | 0666'
	if( (shmid = shmget(mykey, sizeof(SEG_DATA), IPC_CREAT | 0666)) < 0 ) {
		perror("shmget: cannot create shared memory segment; exiting now");
		exit(1);
		}

	//map the segment into our process address space via`shmat' [attach] system call.
	if( (shm = (SEG_DATA *)shmat( shmid, NULL, 0)) == ((SEG_DATA *)-1) ) {
		perror("shmat: cannot attach shared memory segment; exiting now");
		exit(1);
		}


    // attempt to lock shared memory, sleep until we can
    mydata = shm;
    while (mydata->mylock == 1) {
        fprintf(stdout, "Shared memory is beeing used by an other proccess\n");
        sleep(1);
    } //fprintf(stdout, "got access\n");

    mydata->mylock = 1;                                     // lock shared memory
    switch(mydata->present) {
        case 0:
            mydata->present = CLIENT_1;                     // initialise present value when first client starts
            break;
        default:                                            //increase the present var so server will wait for all clients
            mydata->present = mydata->present << 1;
    }
    if (mydata->present == CLIENT_4) mydata->exit = 1;
    mydata->mylock = 0;                                     // unlock shared memory

    // main code display data, and check input for exit signal
 	fprintf(stdout, "Client procces reading from server\n");
	char  input;
	do {
	 sleep(1);
	 fprintf(stdout, "Client           = %d\n", mydata->present);
	 fprintf(stdout, "UP Status        = %d\n", up );
	 fprintf(stdout, "Exit Status      = %d\n", mydata->exit );
	 fprintf(stdout, "RPM              = %d\n", mydata->rpm );
	 fprintf(stdout, "Crank Angle      = %d\n", mydata->crankangle );
	 fprintf(stdout, "Throttle Setting = %d\n", mydata->throttle );
	 fprintf(stdout, "Fuel Flow        = %d\n", mydata->fuelflow );
	 fprintf(stdout, "Engine Temp      = %d\n", mydata->temp );
	 fprintf(stdout, "Fan Speed        = %d\n", mydata->fanspeed );
	 fprintf(stdout, "Oil Pressure     = %d\n", mydata->oilpres );
	 fprintf(stdout, "present          = %d\n", mydata->present );
	 fprintf(stdout, "Enter 1 to exit\n");

	 input = (char) getchar();
	 if (input == '1') 	{ mydata->exit = 1; }
    } while (mydata->exit != 1);

    // get access
    while (mydata->mylock == 1) {
        fprintf(stdout, "Shared memory is being used by an other proccess\n");
        sleep(1);
    } //fprintf(stdout, "got access\n");

    mydata->mylock = 1;                                 // lock shared memory
                                                        // backtrack the present var so server will wait for all clients b4 exiting
    mydata->present = mydata->present >> 1;
    mydata->mylock = 0;                                 // unlock shared memory

	// unmap segment
   if( (shmid = shmdt( shm )) == (-1) ) {
	 perror("shmdt: cannot detach shared memory segment; exiting now");
			exit(1);
	}
   // get rid of the ipcs shared memory, so that values dont perssist
	shmctl(shmid, IPC_RMID, 0);
	fprintf(stdout, "Task completed\n");
	exit(0);
}

