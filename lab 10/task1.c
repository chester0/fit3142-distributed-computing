/*
 * BSD Sockets client - Lab 10, Week 11
 * MPI message passing interface open mpirun
 * build: mpicc <filename>
 * run:   mpirun ^
 * Demetrios Christou
 * mpi send and recieve i+1
 * */
#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv)
{
  int rank, input, world_size, i, tag, next, from, count;
  const int root = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  tag = world_size / 2;
  next = (rank + 1) % world_size;
  from = (rank + world_size - 1) % world_size;

  // root proccess get int from user

  while (1) {
    // get integer from 'from' process into input
    MPI_Recv(&input, 1, MPI_INT, from, tag, MPI_COMM_WORLD, NULL);
    printf("proccess %d recived: %d ...\n", rank, input);
    // Send integer 'num' to 'next' process
    MPI_Send(&input, 1, MPI_INT, next, tag, MPI_COMM_WORLD);
    // The last process does one extra send to process 0, which needs to
    // be received before the program can exit
    if (rank == world_size)
      MPI_Recv(&input, 1, MPI_INT, from, tag, MPI_COMM_WORLD, NULL);
      goto end;
  }
  end:
  MPI_Finalize();
  return 0;
}
