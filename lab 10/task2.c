/*
 * BSD Sockets client - Lab 10, Week 11
 * MPI message passing interface open mpirun
 * build: mpicc <filename>
 * run:   mpirun ^
 * Demetrios Christou
 * mpi send and recieve i+1
 * */
#include <mpi.h>
#include <stdio.h>
#include<mpi.h>
#include<stdio.h>
#include "mpi.h"
#include <stdio.h>


int main(int argc, char *argv[])
{

    int  i, tag=0, world_size, input, right, left, false = 0, rank;
    const int root = 0;

    MPI_Status status;
    MPI_Comm cartcomm;   // required variable

     MPI_Init(&argc,&argv);
     MPI_Comm_size(MPI_COMM_WORLD, &world_size);
     MPI_Cart_create(MPI_COMM_WORLD, 1, &world_size, &false, 1, &cartcomm);
     MPI_Cart_shift(cartcomm, 0, 1, &left, &right);
     MPI_Comm_rank(cartcomm, &rank);
     MPI_Comm_size(cartcomm, &world_size);
     printf("rank: %d, destination: %d\n", rank, right);

    do {
      if(rank == root) {
          printf("enter integer to be send arround\n");
          scanf("%d", &input);
          MPI_Send(&input, 1, MPI_INT, right, tag, cartcomm);
      } else {
        MPI_Recv(&input, 1, MPI_INT, left, tag, cartcomm, NULL);
        printf("proccess %d recived: %d ...\n", rank, input);
        MPI_Send(&input, 1, MPI_INT, right, tag, cartcomm);
      }
    } while (input >= 0);

  MPI_Finalize();
  return 0;

}
