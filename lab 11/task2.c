/* Lab 11, Week 12
 * Demetrios Christou
 * all processes except process 0 send 100 messages to process 0.Have
 * process 0 print out the messages as it receives them,
 * using MPI_ANY_SOURCE and MPI_ANY_TAG in MPI_Recv.
 * Is the MPI implementation fair? -- nope random order of procces and mesages,
 * */
#include <mpi.h>
#include <stdio.h>
int main(int argc, char *argv[])
{
  int  i, world_size, buffer = 0, rank;
  const int master = 0;

  MPI_Status status;
  MPI_Comm cartcomm;   // required variable, why cant use here?
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  if(rank == master) { // rank 0
    // receive 100 times the world size mesges
    for(i=0; i<100*(world_size-1); i++) {
      MPI_Recv(&buffer, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      printf("value: %d from proccess: %d iteration sent: %d \n", buffer,
      status.MPI_SOURCE, status.MPI_TAG);
    }

  } else { // non 0 proccesses, slaves
    ++buffer;

    for(i=0; i<100; i++) {
      MPI_Send(&i, 1, MPI_INT, master, i, MPI_COMM_WORLD);
    }
  }
  MPI_Finalize();
  return 0;
}
