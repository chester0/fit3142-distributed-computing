/*
 * Lab 11, Week 12
 * MPI
 * Demetrios Christou
 * calculate pi
 *
 * The method evaluates the integral of
 * 4/(1+x*x) between 0 and 1.
 *
 * approximation to the integral in each interval
 * (1/n)*4/(1+x*x)
 *
 * each process then adds up every n'th interval
 * x  = rank/n,rank/n+size/n,...
 *
 * then summed up with mpi_reduce
 * */
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main(int argc, char *argv[])
{
  int interval_count, rank, world_size, i, first_ran;
  const double PI = 3.141592653589793238462643383279502884197169399375105820974944592307816406286;
  double local_pi, pi_calculated, h, sum, x, tick, tock;
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&world_size);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  if (first_ran == 0) { //just to have a msg on the first run
    printf("enter interval count, <= 0 to exit\n");
    first_ran = 1;
  }

  while (1) {
    if (rank == 0) {
	    printf("enter interval count, <= 0 to exit\n");
	    scanf("%d",&interval_count);
	  }
    clock_t begin = clock(); // start timer
    tick = MPI_Wtime();     // mpi timer

    // broadcasr
	MPI_Bcast(&interval_count, 1, MPI_INT, 0, MPI_COMM_WORLD);

	if (interval_count <= 0) break; // exit condition

    // each process calculation
	h   = 1.0 / (double) interval_count;
	sum = 0.0;
	for (i = rank + 1; i <= interval_count; i += world_size) {
	    x = h * ((double)i - 0.5);
	    sum += 4.0 / (1.0 + x*x);
	}
	local_pi = h * sum;
    // sum up
	MPI_Reduce(&local_pi, &pi_calculated, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	if (rank == 0) {
        clock_t end = clock();
        tock = MPI_Wtime();
        double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
        printf("\ncalculated pi: %.32f\nreal pi:       %.32f\ndifference:    %.32f\ntime spent:    %.10f\nMPI Timer:     %.10f\n\n",
               pi_calculated, PI, fabs(pi_calculated - PI), time_spent, fabs(tick-tock));
    }
  }
  MPI_Finalize();
  return 0;
}
