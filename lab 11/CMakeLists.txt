cmake_minimum_required(VERSION 3.6)
project(lab_11)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
    Makefile
    task1.c
    task2.c)

add_executable(lab_11 ${SOURCE_FILES})