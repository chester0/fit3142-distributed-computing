/*
 * Shared Memory Client Process
 * 24538817-shm-clientclinet.c
 * Demetrios Christou
 * moddified template Monash uni FIT3142	Lab 3 Week 4
 * reads values from status-shm-server and displays to output
 * server waits for data->exit = 1 to terminate
 */
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "segment.h"

int main()
{
	char
		ident[] = "$Id: 24538817-shm-client.c,v 0.1 2016/08/13 06:53:03 - chester0 $\n";
	int
		up = 0, shmid;
	key_t
		mykey;
	void *
    shmat(int shmid, const void *addr, int flag);
	int
		shmdt(const void *shmaddr);

	SEG_DATA
		*shm, *mydata;

  // The shared memory segment is identified by SEGMENTID
	mykey = SEGMENTID;

  //create the shared memory segment via `shmget' system call.
  //for debugging remove the IPC_EXCL flag - `IPC_CREAT | IPC_EXCL | 0666'
	if( (shmid = shmget(mykey, sizeof(SEG_DATA), IPC_CREAT | 0666)) < 0 ) {
		perror("shmget: cannot create shared memory segment; exiting now");
		exit(1);
		}

	//map the segment into our process address space via`shmat' [attach] system call.
	if( (shm = (SEG_DATA *)shmat( shmid, NULL, 0)) == ((SEG_DATA *)-1) ) {
		perror("shmat: cannot attach shared memory segment; exiting now");
		exit(1);
		}

	// main code display data, and check input for exit signal
 	fprintf(stdout, "Client procces reading from server\n");

	mydata = (SEG_DATA *)shm;
	char  input;
	while (mydata->exit != 1) {
	 sleep(1);
	 fprintf(stdout, "\nSTATUS DUMP\n");
	 fprintf(stdout, "UP Status        = %d\n", up );
	 fprintf(stdout, "Exit Status      = %d\n", mydata->exit );
	 fprintf(stdout, "RPM              = %d\n", mydata->rpm );
	 fprintf(stdout, "Crank Angle      = %d\n", mydata->crankangle );
	 fprintf(stdout, "Throttle Setting = %d\n", mydata->throttle );
	 fprintf(stdout, "Fuel Flow        = %d\n", mydata->fuelflow );
	 fprintf(stdout, "Engine Temp      = %d\n", mydata->temp );
	 fprintf(stdout, "Fan Speed        = %d\n", mydata->fanspeed );
	 fprintf(stdout, "Oil Pressure     = %d\n", mydata->oilpres );
	 fprintf(stdout, "Enter 1 to exit\n");
	 input = getchar();
	 if (input == '1') mydata->exit = 1; // signal to server to exit
  }

	// unmap segment
  if( (shmid = shmdt( shm )) == (-1) ) {
	 perror("shmdt: cannot detach shared memory segment; exiting now");
			exit(1);
	}

	fprintf(stdout, "Task completed\n");
	exit(0);
}
