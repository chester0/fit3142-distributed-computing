#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
int main(int argc, char **argv)
{	
char *fd = "pointers.c";
struct stat buf; 
char buffer[20];
char mode_str[20];

stat(fd, &buf);
int size = buf.st_size;
int id = buf.st_dev;
int uid = buf.st_uid;
int gid = buf.st_gid;
int rdev = buf.st_rdev;
int blksize = buf.st_blksize;
int blocks = buf.st_blocks;
int nlinks = buf.st_nlink;
mode_t mode = buf.st_mode;
strmode(mode, mode_str);
time_t a_time = buf.st_atime;
time (&a_time);
time_t m_time = buf.st_mtime;
time (&m_time);
time_t c_time = buf.st_ctime;
time (&c_time);

printf("st_size: %d\n", size);
printf("id: %d\n", id);
printf("uid: %d\n", uid);
printf("gid: %d\n", gid);
printf("rdev: %d\n", rdev);
printf("blksize: %d\n", blksize);
printf("blocks: %d\n", blocks);
printf("nlinks: %d\n", nlinks);
printf("mode: %s\n", mode_str);
printf("atime: %s", ctime(&a_time));
printf("mtime: %s", ctime(&m_time));
printf("ctime: %s", ctime(&c_time));
}