/*
 * BSD Sockets status server - Lab 6, Week 7
 *
 * Demetrios Christou
 *
 * */

#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include "segment.h"

#define MAX_SIZE 2000

int main(int argc , char *argv[])
{
    int client_socket, c, up = 0;
    struct sockaddr_in server, client;
    char client_message[MAX_SIZE];


    SEG_DATA *mydata;


    //create socket, TCP, IPV4
    client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket == -1) {
        perror("create socket");
    }
    puts("socket created");

    // configure struct, any inc address, TCP, IPV4, port
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(8888);

    //Bind
    if (bind(client_socket,(struct sockaddr *)&server , sizeof(server)) < 0) {
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");

    //Listen
    listen(client_socket, 3);

    //Accept and incoming connection
    puts("Waiting for incoming connections ");
    c = sizeof(struct sockaddr_in);

    //accept connection from an incoming client
    client_socket = accept(client_socket, (struct sockaddr *)&client, (socklen_t*)&c);
    if (client_socket < 0)  {
        perror("accept failed");
        return 1;
    }
    puts("Connection accepted ");

    // Initialise values
    mydata = malloc(sizeof(SEG_DATA));
    mydata->exit = 0;
    mydata->rpm = 3500;
    mydata->crankangle = 0;
    mydata->throttle = 70;
    mydata->fuelflow = 50;
    mydata->temp = 80;
    mydata->fanspeed = 30;
    mydata->oilpres = 70;

    while (mydata->exit != 1) {
        if (up == 1 && mydata->rpm > 6500) up = 0; /* cycle values */
        if (up == 0 && mydata->rpm < 500) up = 1;
        if (up == 1) {
            mydata->rpm += 100;
            mydata->crankangle += 1;
            mydata->crankangle %= 360;
            mydata->throttle += 1;
            mydata->throttle %= 100;
            mydata->fuelflow += 1;
            mydata->fuelflow %= 100;
            mydata->temp += 1;
            mydata->temp %= 100;
            mydata->fanspeed += 1;
            mydata->fanspeed %= 100;
            mydata->oilpres += 1;
            mydata->oilpres %= 100;
        } else {
            mydata->rpm -= 100;
            mydata->crankangle -= 1;
            mydata->crankangle %= 360;
            mydata->throttle -= 1;
            mydata->throttle %= 100;
            mydata->fuelflow -= 1;
            mydata->fuelflow %= 100;
            mydata->temp -= 1;
            mydata->temp %= 100;
            mydata->fanspeed -= 1;
            mydata->fanspeed %= 100;
            mydata->oilpres -= 1;
            mydata->oilpres %= 100;
        }
        sleep(1);
        fprintf(stdout, "\nSTATUS DUMP\n");
        fprintf(stdout, "UP Status        = %d\n", up);
        fprintf(stdout, "Exit Status      = %d\n", mydata->exit);
        fprintf(stdout, "RPM              = %d\n", mydata->rpm);
        fprintf(stdout, "Crank Angle      = %d\n", mydata->crankangle);
        fprintf(stdout, "Throttle Setting = %d\n", mydata->throttle);
        fprintf(stdout, "Fuel Flow        = %d\n", mydata->fuelflow);
        fprintf(stdout, "Engine Temp      = %d\n", mydata->temp);
        fprintf(stdout, "Fan Speed        = %d\n", mydata->fanspeed);
        fprintf(stdout, "Oil Pressure     = %d\n", mydata->oilpres);
        fprintf(stdout, "Waiting for client\n");

        unsigned char bytes[4];
        int size = 0;
        int i;
        // Setup the string to be sent
        // ---- UP Value
        bytes[0] = (unsigned char) ((up >> 24) & 0xFF);
        bytes[1] = (unsigned char) ((up >> 16) & 0xFF);
        bytes[2] = (unsigned char) ((up >> 8) & 0xFF);
        bytes[3] = (unsigned char) (up & 0xFF);
        for (i = 0; i < 4; i++)
            client_message[size++] = bytes[i];

        // ---- Exit Value
        bytes[0] = (unsigned char) ((mydata->exit >> 24) & 0xFF);
        bytes[1] = (unsigned char) ((mydata->exit >> 16) & 0xFF);
        bytes[2] = (unsigned char) ((mydata->exit >> 8) & 0xFF);
        bytes[3] = (unsigned char) (mydata->exit & 0xFF);
        for (i = 0; i < 4; i++)
            client_message[size++] = bytes[i];

        // ---- RPM
        bytes[0] = (unsigned char) ((mydata->rpm >> 24) & 0xFF);
        bytes[1] = (unsigned char) ((mydata->rpm >> 16) & 0xFF);
        bytes[2] = (unsigned char) ((mydata->rpm >> 8) & 0xFF);
        bytes[3] = (unsigned char) (mydata->rpm & 0xFF);
        for (i = 0; i < 4; i++)
            client_message[size++] = bytes[i];

        // ---- Crank Angle
        bytes[0] = (unsigned char) ((mydata->crankangle >> 24) & 0xFF);
        bytes[1] = (unsigned char) ((mydata->crankangle >> 16) & 0xFF);
        bytes[2] = (unsigned char) ((mydata->crankangle >> 8) & 0xFF);
        bytes[3] = (unsigned char) (mydata->crankangle & 0xFF);
        for (i = 0; i < 4; i++)
            client_message[size++] = bytes[i];

        // ---- Throttle
        bytes[0] = (unsigned char) ((mydata->throttle >> 24) & 0xFF);
        bytes[1] = (unsigned char) ((mydata->throttle >> 16) & 0xFF);
        bytes[2] = (unsigned char) ((mydata->throttle >> 8) & 0xFF);
        bytes[3] = (unsigned char) (mydata->throttle & 0xFF);
        for (i = 0; i < 4; i++)
            client_message[size++] = bytes[i];

        // ---- Fuel FLow
        bytes[0] = (unsigned char) ((mydata->fuelflow >> 24) & 0xFF);
        bytes[1] = (unsigned char) ((mydata->fuelflow >> 16) & 0xFF);
        bytes[2] = (unsigned char) ((mydata->fuelflow >> 8) & 0xFF);
        bytes[3] = (unsigned char) (mydata->fuelflow & 0xFF);
        for (i = 0; i < 4; i++)
            client_message[size++] = bytes[i];

        // ---- Temp
        bytes[0] = (unsigned char) ((mydata->temp >> 24) & 0xFF);
        bytes[1] = (unsigned char) ((mydata->temp >> 16) & 0xFF);
        bytes[2] = (unsigned char) ((mydata->temp >> 8) & 0xFF);
        bytes[3] = (unsigned char) (mydata->temp & 0xFF);
        for (i = 0; i < 4; i++)
            client_message[size++] = bytes[i];

        // ---- Fan speed
        bytes[0] = (unsigned char) ((mydata->fanspeed >> 24) & 0xFF);
        bytes[1] = (unsigned char) ((mydata->fanspeed >> 16) & 0xFF);
        bytes[2] = (unsigned char) ((mydata->fanspeed >> 8) & 0xFF);
        bytes[3] = (unsigned char) (mydata->fanspeed & 0xFF);
        for (i = 0; i < 4; i++)
            client_message[size++] = bytes[i];

        // ---- Oil pressure
        bytes[0] = (unsigned char) ((mydata->oilpres >> 24) & 0xFF);
        bytes[1] = (unsigned char) ((mydata->oilpres >> 16) & 0xFF);
        bytes[2] = (unsigned char) ((mydata->oilpres >> 8) & 0xFF);
        bytes[3] = (unsigned char) (mydata->oilpres & 0xFF);
        for (i = 0; i < 4; i++)
            client_message[size++] = bytes[i];

        //Send the data, check for return value
        if (send(client_socket, client_message, (size_t) size, 0) < 0) {
            puts("Send failed");
            return 1;
        }
        memset(client_message, 0, sizeof client_message);                   // clear contents of message for new one
        sleep(2);
    }
    return 0;
}
