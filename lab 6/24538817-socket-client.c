/*
 * BSD Sockets client - Lab 6, Week 7
 * bsd-client.c
 * Demetrios Christou
 *
 * */

#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include "segment.h"

int main(int argc , char *argv[]) {
    int my_socket;
    struct sockaddr_in server;
    char message[2000];
    SEG_DATA *mydata = malloc(sizeof(SEG_DATA));

    //Create socket - AF_INET = IPV4, SOCK_STREAM = TCP
    my_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (my_socket == -1) {
        printf("Could not create socket");
    }
    puts("Socket created");

    // CONFIGURE SERVER STRUCT, IPV4, lochalhost for ip, and IPV4M, PORT = 8888
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons(8888);

    //Connect to remote server
    if (connect(my_socket, (struct sockaddr *) &server, sizeof(server)) < 0) {
        perror("connect failed. Error");
        return 1;
    }
    puts("Connected\n");

    // MAIN LOOP
    do {
        sleep(1);
        memset(message, 0, 2000);                       // clear contents of message for new one
        int read_size = (int) recv(my_socket, message, 2000, 0);  // receive from client and check return value for error
        if(read_size == 0) {
            puts("server disconnected, exiting....");
            return 2;
        }
        else if(read_size == -1) {
            perror("receive failed");
            return 3;
        }

        int up =                message[0] << 24 | message[1] << 16 | message[2] << 8 | message[3];
        mydata->exit =          message[4] << 24 | message[5] << 16 | message[6] << 8 | message[7];
        mydata->rpm =           message[8] << 24 | message[9] << 16 | message[10] << 8 | message[11];
        mydata->crankangle =    message[12] << 24 | message[13] << 16 | message[14] << 8 | message[15];
        mydata->throttle =      message[16] << 24 | message[17] << 16 | message[18] << 8 | message[19];
        mydata->fuelflow =      message[20] << 24 | message[21] << 16 | message[22] << 8 | message[23];
        mydata->temp =          message[24] << 24 | message[25] << 16 | message[26] << 8 | message[27];
        mydata->fanspeed =      message[28] << 24 | message[29] << 16 | message[30] << 8 | message[31];
        mydata->oilpres =       message[32] << 24 | message[33] << 16 | message[34] << 8 | message[35];

        sleep(1);
        fprintf(stdout, "\nCLIENT STATUS DUMP\n");
        fprintf(stdout, "UP Status        = %d\n", up );
        fprintf(stdout, "Exit Status      = %d\n", mydata->exit );
        fprintf(stdout, "RPM              = %d\n", mydata->rpm );
        fprintf(stdout, "Crank Angle      = %d\n", mydata->crankangle );
        fprintf(stdout, "Throttle Setting = %d\n", mydata->throttle );
        fprintf(stdout, "Fuel Flow        = %d\n", mydata->fuelflow );
        fprintf(stdout, "Engine Temp      = %d\n", mydata->temp );
        fprintf(stdout, "Fan Speed        = %d\n", mydata->fanspeed );
        fprintf(stdout, "Oil Pressure     = %d\n", mydata->oilpres );
        fprintf(stdout, "Enter 1 to exit\n");
        int input =  getchar();
        if (input == 1) mydata->exit = 1; // signal to server to exit


    } while (mydata->exit != 1);                       // while we don't have end of file as first char

    close(my_socket);                                   // close the socket
    return 0;
}
