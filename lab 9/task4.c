/*
 *  Lab 9, Week 10
 * MPI PACK
 * build: mpicc <filename>
 * run:   mpirun ^
 * Demetrios Christou
 * */
 #include <mpi.h>
 #include <stdio.h>
 #include <stddef.h>

int main(int argc, char** argv) {
  int rank, pack_size = 0, position, my_integer;
  double my_double;
  char packed_buffer[100];
  const int root = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  // MAIN LOOP
  do {
    if(rank == root) {
      printf("enter integer, negative to exit, then float\n");
      fflush(stdin);
      scanf("%d %lf", &my_integer, &my_double);
      pack_size = 0;
      // Pack values
      MPI_Pack(&my_integer, 1, MPI_INT, packed_buffer, 100, &pack_size, MPI_COMM_WORLD);
      MPI_Pack(&my_double, 1, MPI_DOUBLE, packed_buffer, 100, &pack_size, MPI_COMM_WORLD);
    } // Lower ranks
    MPI_Bcast(&pack_size, 1, MPI_INT, root, MPI_COMM_WORLD);
    MPI_Bcast(&packed_buffer, pack_size, MPI_PACKED, root, MPI_COMM_WORLD);
    if (rank != 0) {
      position = 0;
      MPI_Unpack(packed_buffer, pack_size, &position, &my_integer, 1, MPI_INT, MPI_COMM_WORLD);
      MPI_Unpack(packed_buffer, pack_size, &position, &my_double, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    }
    printf("rank: %d , integer is: %d float is: %lf\n", rank, my_integer, my_double);
    fflush(stdin);
    } while (my_integer > 0);

    MPI_Finalize();
    return 0;
 }
