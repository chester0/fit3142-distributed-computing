/*
 * Lab 9, Week 10
 * MPI BROADCAST
 * build: mpicc <filename>
 * run:   mpirun ^
 * Demetrios Christou
 * */

#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv)
{
  int rank, buf;
  const int root = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  // MAIN LOOP
  do {
    if(rank == root) {
      printf("enter input, negative to exit\n");
      fflush(stdout);
      scanf("%d", &buf);
    }
  MPI_Bcast(&buf, 1, MPI_INT, root, MPI_COMM_WORLD);
  printf("rank: %d , input is %d\n", rank, buf);
  fflush(stdout);
  } while (buf > 0);

  MPI_Finalize();
  return 0;
}
