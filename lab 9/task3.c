/*
 * Lab 9, Week 10
 * MPI PACKED MESSAGES
 * build: mpicc <filename>
 * run:   mpirun ^
 * Demetrios Christou
 * */
 #include <mpi.h>
 #include <stdio.h>

int main(int argc, char** argv)
{
  struct {
    int my_integer;
    double my_double;
  } values;

  const int root = 0;
  int num_of_items = 2, rank;
  MPI_Aint indices[2];
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Datatype types[2], my_struct;
  int  blocklengths[2] = {1, 1};                // 2 values 1 of each
  types[0] = MPI_INT;  types[1] = MPI_DOUBLE;   //The base types
  MPI_Address(&values.my_integer, &indices[0]);
  MPI_Address(&values.my_double, &indices[1]);

  // make relative
  indices[1] = indices[1] - indices[0];
  indices[0] = 0;

  MPI_Type_struct(num_of_items, blocklengths, indices, types, &my_struct);
  MPI_Type_commit(&my_struct);
  // MAIN LOOP
  do {
    if(rank == root) {
      printf("enter integer, negative to exit\n");
      fflush(stdin);
      scanf("%d %lf", &values.my_integer, &values.my_double);
      }
      MPI_Bcast(&values, 1, my_struct, root, MPI_COMM_WORLD);
      printf("rank: %d , integer is: %d float is: %lf\n", rank, values.my_integer, values.my_double);
      fflush(stdin);
    } while (values.my_integer > 0);

    /* Clean up the type */
    MPI_Type_free( &my_struct );
    MPI_Finalize();
    return 0;
 }
