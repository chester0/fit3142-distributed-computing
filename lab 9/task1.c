/*
 * BSD Sockets client - Lab 9, Week 7
 * MPI message passing interface open mpirun
 * build: mpicc <filename>
 * run:   mpirun ^
 * Demetrios Christou
 *
 * */
#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
    // Initialize the MPI environment
    MPI_Init(&argc, &argv);
    // Get the number of processes
    int process_count;
    MPI_Comm_size(MPI_COMM_WORLD, &process_count);
    // Get the rank
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // processor name
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    printf("Hello World from: Process %d on %s out of %d\n", rank, processor_name, process_count);
    
    MPI_Finalize();
}
